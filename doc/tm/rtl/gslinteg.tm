\function_sect{Numerical Integration Routines}

\function{integration_qng}
\synopsis{non-adaptive Gauss-Kronrod integration}
\usage{res = integration_qng (&func, [optargs-list,] a, b, epsabs, relabs}
\description
  This function wraps the \exmp{gsl_integration_qng} function.
  See the GSL documentation for more information.
#% \seealso{}
\done

\function{integration_qag}
\synopsis{Adaptive integration}
\usage{res = integration_qag (&func, [optargs_list,] a, b, epsabs, relabs, limit, key)}
\description
  This function wraps the \exmp{gsl_integration_qag} function.  The
  paramter \exmp{key} must be set to one of the following symbolic
  constants:
#v+
   GSL_INTEG_GAUSS15
   GSL_INTEG_GAUSS21
   GSL_INTEG_GAUSS31
   GSL_INTEG_GAUSS41
   GSL_INTEG_GAUSS51
   GSL_INTEG_GAUSS61
#v-
  See the GSL documentation for more information.
#% \seealso{}
\done

\function{integration_qags}
\synopsis{Adaptive integration with singularities}
\usage{res = integration_qags (&func, [optargs_list,] a, b, epsabs, relabs, limit)}
\description
  This function wraps the \exmp{gsl_integration_qags} function.
  See the GSL documentation for more information.
#% \seealso{}
\done

\function{integration_qagp}
\synopsis{Adaptive integration with known singular points}
\usage{res = integration_qagp (&func, [optargs_list,] pts, epsabs, epsrel, limit)}
\description
  This function wraps the \exmp{gsl_integration_qagp} function.  Here,
  \exmp{pts} is an array of ordered values \exmp{[a, x1, x2, ..., xn, b]} that
  contain the end-points and the locations \exmp{x1, ..., xn} of
  singularities.
  See the GSL documentation for more information.
#% \seealso{}
\done

\function{integration_qagi}
\synopsis{Adaptive integration on infinite intervals}
\usage{res = integration_qagi (&func, [optargs_list,] epsabs, epsrel, limit)}
\description
  This function wraps the \exmp{gsl_integration_qagi} function.
  See the GSL documentation for more information.
#% \seealso{}
\done

\function{integration_qagiu}
\synopsis{Adaptive integration on semi-infinite intervals}
\usage{res = integration_qagiu (&func, [optargs_list,] a, epsabs, epsrel, limit)}
\description
  This function wraps the \exmp{gsl_integration_qagiu} function.
  See the GSL documentation for more information.
#% \seealso{}
\done

\function{integration_qagil}
\synopsis{Adaptive integration on semi-infinite intervals}
\usage{res = integration_qagil (&func, [optargs_list,] b, epsabs, epsrel, limit)}
\description
  This function wraps the \exmp{gsl_integration_qagil} function.
  See the GSL documentation for more information.
#% \seealso{}
\done

\function{integration_qawc}
\synopsis{Adaptive integration for Cauchy principal values}
\usage{res = integration_qawc (&func, [optargs_list,] a, b, c, epsabs, epsrel, limit)}
\description
  This function wraps the \exmp{gsl_integration_qawc} function.
  See the GSL documentation for more information.
  #% \seealso{}
\done

\function{integration_cquad}
\synopsis{Doubly-adaptive integration}
\usage{res = integration_cquad (&func, [optargs_list,] a, b, epsabs, epsrel, limit)}
\description
  This function wraps the \exmp{gsl_integration_cquad} function.
  See the GSL documentation for more information.
  #% \seealso{}
\done

\function{integration_romberg}
\synopsis{Romberg integration}
\usage{res = integration_romberg (&func, [optargs_list,] a, b, epsabs, epsrel, limit)}
\description
  This function wraps the \exmp{gsl_integration_romberg} function.
  See the GSL documentation for more information.
  #% \seealso{}
\done

\function{integration_fixed_alloc}
\synopsis{Create a workspace for the integration_fixed function}
\usage{fixedobj = integration_fixed_alloc (typestr, n, a, b, alpha, beta)}
\description
  This function wraps the \exmp{gsl_integration_fixed_alloc} function.
  It return a workspace that is to be passed to the
\ifun{integration_fixed} function.  The \exmp{typestr} parameter is
  used to specify the weighting functions using by the integrator.  It
  must be one of the following:
  #v+
     "legendre", "chebyshev", "gegenbauer", "jacobi", "laguerre",
     "hermite", "exponential", "rational", "chebyshev2"
  #v-
  See the GSL documentation for more information.
  #% \seealso{}
\done

\function{integration_fixed}
\synopsis{Fixed point quadrature integration}
\usage{res = integration_fixed (&func, [optargs_list,] fixedobj)}
\description
  This function wraps the \exmp{gsl_integration_fixed} function.  Here,
\exmp{fixedobj} is the workspace returned by the
\exmp{integration_fixed_alloc} function.
  See the GSL documentation for more information.
  #% \seealso{}
\done

\function{integration_glfixed_alloc}
\synopsis{Create a table of precomputed values for the integration_fixed function}
\usage{glfixed_table = integration_glfixed_alloc (n)}
\description
  This function wraps the \exmp{gsl_integration_glfixed_table_alloc}
  function.  It returns a table of values that is to be passed to the
\ifun{integration_glfixed} function to perform an n-point fixed order
  integration.

  See the GSL documentation for more information.
  #% \seealso{}
\done

\function{integration_glfixed}
\synopsis{Gauss-Legendre integration}
\usage{res = integration_glfixed (&func, [optargs_list,] a, b, glfixed_table)}
\description
  This function wraps the \exmp{gsl_integration_glfixed} function.  The
\exmp{glfixed_table} represents the precomputed values returned by the
\ifun{integration_glfixed_alloc} function.

  See the GSL documentation for more information.
  #% \seealso{}
\done

\function{integration_qaws_alloc}
\synopsis{Create a table of precomputed values for the integration_qaws function}
\usage{qaws_table = integration_qaws_alloc (alpha, beta, mu, nu)}
\description
  This function wraps the \exmp{gsl_integration_qaws_table_alloc}
  function.  It returns a precomputed table of values that is to be
  passed to the \ifun{integration_qaws} function.

  See the GSL documentation for more information.
  #% \seealso{}
\done

\function{integration_qaws}
\synopsis{Adaptive integration for singular functions}
\usage{res = integration_qaws (&func, [optargs_list,] a, b, epsabs, epsrel, limit, qaws_table)}
\description
  This function wraps the \exmp{gsl_integration_qaws} function.
  Here, \exmp{qaws_table} represents the pre-computed values returned by the
\exmp{integration_qaws_alloc} function.

  See the GSL documentation for more information.
  #% \seealso{}
\done

\function{integration_qawo_alloc}
\synopsis{Create a table of precomputed values for the integration_qawo function}
\usage{res = integration_qawo_alloc (omega, L, type, n)}
\description
  This function wraps the \exmp{gsl_integration_qawo_table_alloc}
  function. It returns a precomputed table of Chebyshev moments that is
  to be passed to the \ifun{integration_qawo} function.  Here,
\exmp{type} must be one of the symbolic constants
\exmp{GSL_INTEG_COSINE} or \exmp{GSL_INTEG_SINE}.

  See the GSL documentation for more information.
  #% \seealso{}
\done

\function{integration_qawo}
\synopsis{Adaptive integration for oscillatory functions}
\usage{res = integration_qawo (&func, [optargs_list,] a, epsabs, epsrel, limit, qawo_table)}
\description
  This function wraps the \exmp{gsl_integration_qawo} function.  Here,
\exmp{qawo_table} is to be precomputed using the
\ifun{integration_qawo_alloc} function.

  See the GSL documentation for more information.
  #% \seealso{}
\done

\function{integration_qawf}
\synopsis{Adaptive integration for Fourier integrals}
\usage{res = integration_qawf (&func, [optargs_list, a, epsabs, limit, qawo_table] )}
\description
  This function wraps the \exmp{gsl_integration_qawf} function.
  Here, \exmp{qawo_table} is to be precomputed using the
\ifun{integration_qawo_alloc} function.
  See the GSL documentation for more information.
  #% \seealso{}
\done

