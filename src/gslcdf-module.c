/* -*- mode: C; mode: fold; -*- */
/* This file was automatically generated. */

/*
  Copyright (c) 2003-2011 Massachusetts Institute of Technology

  This software was developed by the MIT Center for Space Research
  under contract SV1-61010 from the Smithsonian Institution.
  
  Permission to use, copy, modify, distribute, and sell this software
  and its documentation for any purpose is hereby granted without fee,
  provided that the above copyright notice appear in all copies and
  that both that copyright notice and this permission notice appear in
  the supporting documentation, and that the name of the Massachusetts
  Institute of Technology not be used in advertising or publicity
  pertaining to distribution of the software without specific, written
  prior permission.  The Massachusetts Institute of Technology makes
  no representations about the suitability of this software for any
  purpose.  It is provided "as is" without express or implied warranty.
  
  THE MASSACHUSETTS INSTITUTE OF TECHNOLOGY DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS.  IN NO EVENT SHALL THE MASSACHUSETTS
  INSTITUTE OF TECHNOLOGY BE LIABLE FOR ANY SPECIAL, INDIRECT OR
  CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
  OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
  NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
  WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

/* Author: John E. Davis (jed@jedsoft.org) */

#include <stdio.h>
#include <slang.h>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_version.h>

#include <gsl/gsl_cdf.h>

#ifdef __cplusplus
extern "C"
{
#endif
/* SLANG_MODULE(gslcdf); */
#ifdef __cplusplus
}
#endif

#include "slgsl.h"
#include "version.h"

#define MODULE_HAS_INTRINSICS
#define _GSLCDF_MODULE_C_
#ifdef MODULE_HAS_INTRINSICS
/*{{{ Helper Functions */

#ifdef _GSLSF_MODULE_C_
static gsl_mode_t Default_GSL_Mode = GSL_PREC_SINGLE;

static int get_gsl_precision (void)
{
   return (int) Default_GSL_Mode;
}
static void set_gsl_precision (int *pp)
{
   int p = *pp;

   if ((p == GSL_PREC_SINGLE) || (p == GSL_PREC_DOUBLE) || (p == GSL_PREC_APPROX))
     Default_GSL_Mode = p;
}

static int get_gsl_mode (gsl_mode_t *mp, int from_stack)
{
   if (from_stack)
     {
	int mode;
	if (-1 == SLang_pop_integer (&mode))
	  return -1;
	*mp = (gsl_mode_t) mode;
     }

   *mp = Default_GSL_Mode;
   return 0;
}

static void do_d_dm (double (*f)(double, gsl_mode_t), gsl_mode_t m)
{
   SLGSL_Double_Array_Type a;
   SLang_Array_Type *in, *out;
   unsigned int i, n;
   double *xp, *yp;

   if (-1 == slgsl_pop_d_array (&a, 0))
     return;

   if (NULL == (in = a.at))
     {
	(void) SLang_push_double ((*f)(a.x, m));
	return;
     }

   if (NULL == (out = SLang_create_array (SLANG_DOUBLE_TYPE, 0, NULL, in->dims, in->num_dims)))
     {
	SLang_free_array (in);
	return;
     }

   n = in->num_elements;
   xp = a.xp;
   yp = (double *) out->data;
   for (i = 0; i < n; i++)
     yp[i] = (*f)(xp[i], m);

   (void) SLang_push_array (out, 1);
   SLang_free_array (in);
}

static void do_d_ddm (double (*f)(double, double, gsl_mode_t), gsl_mode_t m)
{
   SLGSL_Double_Array_Type a, b;
   SLang_Array_Type *atz;
   unsigned int i, n;
   double *xp, *yp, *zp;
   unsigned int xinc, yinc;

   if (-1 == slgsl_pop_dd_array (&a, &b, 0))
     return;

   if ((NULL == (atz = a.at))
       && (NULL == (atz = b.at)))
     {
	(void) SLang_push_double ((*f)(a.x, b.x, m));
	return;
     }

   atz = SLang_create_array (SLANG_DOUBLE_TYPE, 0, NULL, atz->dims, atz->num_dims);
   if (atz == NULL)
     {
	SLang_free_array (a.at);
	SLang_free_array (b.at);
	return;
     }

   n = atz->num_elements;
   zp = (double *) atz->data;
   xp = a.xp;
   yp = b.xp;
   xinc = a.inc;
   yinc = b.inc;

   for (i = 0; i < n; i++)
     {
	zp[i] = (*f)(*xp, *yp, m);
	xp += xinc;
	yp += yinc;
     }

   (void) SLang_push_array (atz, 1);
   SLang_free_array (a.at);
   SLang_free_array (b.at);
}

static void do_d_dddm (double (*f)(double, double, double, gsl_mode_t), gsl_mode_t m)
{
   SLGSL_Double_Array_Type a, b, c;
   SLang_Array_Type *atz;
   unsigned int i, n;
   double *ap, *bp, *cp, *zp;
   unsigned int ainc, binc, cinc;

   if (-1 == slgsl_pop_ddd_array (&a, &b, &c, 0))
     return;

   if ((NULL == (atz = a.at))
       && (NULL == (atz = b.at))
       && (NULL == (atz = c.at)))
     {
	(void) SLang_push_double ((*f)(a.x, b.x, c.x, m));
	return;
     }

   atz = SLang_create_array (SLANG_DOUBLE_TYPE, 0, NULL, atz->dims, atz->num_dims);
   if (atz == NULL)
     {
	SLang_free_array (a.at);
	SLang_free_array (b.at);
	SLang_free_array (c.at);
	return;
     }

   n = atz->num_elements;
   zp = (double *) atz->data;
   ap = a.xp;
   bp = b.xp;
   cp = c.xp;
   ainc = a.inc;
   binc = b.inc;
   cinc = c.inc;

   for (i = 0; i < n; i++)
     {
	zp[i] = (*f)(*ap, *bp, *cp, m);
	ap += ainc;
	bp += binc;
	cp += cinc;
     }

   (void) SLang_push_array (atz, 1);
   SLang_free_array (a.at);
   SLang_free_array (b.at);
   SLang_free_array (c.at);
}

static void do_d_ddddm (double (*f)(double, double, double, double, gsl_mode_t),
			gsl_mode_t m)
{
   SLGSL_Double_Array_Type a, b, c, d;
   SLang_Array_Type *atz;
   unsigned int i, n;
   double *ap, *bp, *cp, *dp, *zp;
   unsigned int ainc, binc, cinc, dinc;

   if (-1 == slgsl_pop_dddd_array (&a, &b, &c, &d, 0))
     return;

   if ((NULL == (atz = a.at))
       && (NULL == (atz = b.at))
       && (NULL == (atz = c.at))
       && (NULL == (atz = d.at)))
     {
	(void) SLang_push_double ((*f)(a.x, b.x, c.x, d.x, m));
	return;
     }

   atz = SLang_create_array (SLANG_DOUBLE_TYPE, 0, NULL, atz->dims, atz->num_dims);
   if (atz == NULL)
     {
	SLang_free_array (a.at);
	SLang_free_array (b.at);
	SLang_free_array (c.at);
	SLang_free_array (d.at);
	return;
     }

   n = atz->num_elements;
   zp = (double *) atz->data;
   ap = a.xp;
   bp = b.xp;
   cp = c.xp;
   dp = d.xp;
   ainc = a.inc;
   binc = b.inc;
   cinc = c.inc;
   dinc = d.inc;

   for (i = 0; i < n; i++)
     {
	zp[i] = (*f)(*ap, *bp, *cp, *dp, m);
	ap += ainc;
	bp += binc;
	cp += cinc;
	dp += dinc;
     }

   (void) SLang_push_array (atz, 1);
   SLang_free_array (a.at);
   SLang_free_array (b.at);
   SLang_free_array (c.at);
   SLang_free_array (d.at);
}

static void do_d_dm_fun (const char *fun, double (*f)(double, gsl_mode_t))
{
   gsl_mode_t m;

   if (SLang_Num_Function_Args < 1)
     {
	SLang_verror (SL_USAGE_ERROR, "Usage: y=%s(double[,mode])", fun);
	return;
     }
   if (-1 == get_gsl_mode (&m, SLang_Num_Function_Args-1))
     return;

   slgsl_reset_errors ();
   do_d_dm (f,m);
   slgsl_check_errors (fun);
}

static void do_d_ddm_fun (const char *fun, double (*f)(double, double, gsl_mode_t))
{
   gsl_mode_t m;
   if (SLang_Num_Function_Args < 2)
     {
	SLang_verror (SL_USAGE_ERROR, "Usage: y=%s(double, double [,mode])", fun);
	return;
     }
   if (-1 == get_gsl_mode (&m, SLang_Num_Function_Args-2))
     return;
   slgsl_reset_errors ();
   do_d_ddm (f,m);
   slgsl_check_errors (fun);
}

static void do_d_dddm_fun (const char *fun, double (*f)(double, double, double, gsl_mode_t))
{
   gsl_mode_t m;
   if (SLang_Num_Function_Args < 3)
     {
	SLang_verror (SL_USAGE_ERROR, "Usage: y=%s(double, double, double[,mode])", fun);
	return;
     }
   if (-1 == get_gsl_mode (&m, SLang_Num_Function_Args-3))
     return;
   slgsl_reset_errors ();
   do_d_dddm (f,m);
   slgsl_check_errors (fun);
}

static void do_d_ddddm_fun (const char *fun, double (*f)(double,double,double,double,gsl_mode_t))
{
   gsl_mode_t m;

   if (SLang_Num_Function_Args < 4)
     {
	SLang_verror (SL_USAGE_ERROR, "Usage: y=%s(double, double, double, double [,mode])", fun);
	return;
     }
   if (-1 == get_gsl_mode (&m, SLang_Num_Function_Args-4))
     return;
   slgsl_reset_errors ();
   do_d_ddddm (f,m);
   slgsl_check_errors (fun);
}

static void do_c_c_fun (const char *fun, int (*f)(double, double, gsl_sf_result*, gsl_sf_result*))
{
   SLGSL_Complex_Array_Type a;
   SLang_Array_Type *in, *out;
   unsigned int i, n;
   gsl_sf_result gsl_zr, gsl_zi;
   double *xp, *yp;

   if (SLang_Num_Function_Args < 1)
     {
        SLang_verror (SL_USAGE_ERROR, "Usage: y=%s(complex)", fun);
        return;
     }

   if (-1 == slgsl_pop_c_array (&a, 0))
     return;

   if (NULL == (in = a.at))
     {
        (void) (*f)(a.x[0], a.x[1], &gsl_zr, &gsl_zi);
        (void) SLang_push_complex (gsl_zr.val, gsl_zi.val);
        return;
     }

   if (NULL == (out = SLang_create_array (SLANG_COMPLEX_TYPE, 0, NULL, in->dims, in->num_dims)))
     {
        SLang_free_array (in);
        return;
     }

   n = in->num_elements;
   xp = a.xp;
   yp = (double *) out->data;
   for (i = 0; i < 2*n; i+=2)
     {
        (void) (*f)(xp[i], xp[i+1], &gsl_zr, &gsl_zi);
        yp[i] = gsl_zr.val;
        yp[i+1] = gsl_zi.val;
     }

   (void) SLang_push_array (out, 1);
   SLang_free_array (in);
}


#endif				       /* _GSLSF_MODULE_C_ */

/* Macros to aid in wrapping the functions */
#define SLF(f) f##_intrin

#define D_FD(f,n) \
     static void SLF(f) (void) { slgsl_do_d_d_fun (n,f); }
#define D_FDD(f,n) \
     static void SLF(f) (void) { slgsl_do_d_dd_fun (n,f); }
#define D_FDDD(f,n) \
     static void SLF(f) (void) { slgsl_do_d_ddd_fun (n,f); }
#define D_FDDDD(f,n) \
     static void SLF(f) (void) { slgsl_do_d_dddd_fun (n,f); }
#define D_FDM(f,n) \
     static void SLF(f) (void) { do_d_dm_fun (n,f); }
#define D_FDDM(f,n) \
     static void SLF(f) (void) { do_d_ddm_fun (n,f); }
#define D_FDDDM(f,n) \
     static void SLF(f) (void) { do_d_dddm_fun (n,f); }
#define D_FDDDDM(f,n) \
     static void SLF(f) (void) { do_d_ddddm_fun (n,f); }
#define D_FI(f,n) \
     static void SLF(f) (void) { slgsl_do_d_i_fun (n,f); }
#define D_FID(f,n) \
     static void SLF(f) (void) { slgsl_do_d_id_fun (n,f); }
#define D_FIDD(f,n) \
     static void SLF(f) (void) { slgsl_do_d_idd_fun (n,f); }
#define D_FIID(f,n) \
     static void SLF(f) (void) { slgsl_do_d_iid_fun (n,f); }
#define D_FIIDD(f,n) \
     static void SLF(f) (void) { slgsl_do_d_iidd_fun (n,f); }
#define I_FD(f,n) \
     static void SLF(f) (void) { slgsl_do_i_d_fun (n,f); }
/* Complex wrappers */
#define C_FC(f,n) \
     static void SLF(f) (void) { do_c_c_fun (n,f); }

/*}}}*/

D_FDDD(gsl_cdf_beta_P,"cdf_beta_P")
D_FDDD(gsl_cdf_beta_Pinv,"cdf_beta_Pinv")
D_FDDD(gsl_cdf_beta_Q,"cdf_beta_Q")
D_FDDD(gsl_cdf_beta_Qinv,"cdf_beta_Qinv")
D_FDDD(gsl_cdf_exppow_P,"cdf_exppow_P")
D_FDDD(gsl_cdf_exppow_Q,"cdf_exppow_Q")
D_FDDD(gsl_cdf_fdist_P,"cdf_fdist_P")
D_FDDD(gsl_cdf_fdist_Pinv,"cdf_fdist_Pinv")
D_FDDD(gsl_cdf_fdist_Q,"cdf_fdist_Q")
D_FDDD(gsl_cdf_fdist_Qinv,"cdf_fdist_Qinv")
D_FDDD(gsl_cdf_flat_P,"cdf_flat_P")
D_FDDD(gsl_cdf_flat_Pinv,"cdf_flat_Pinv")
D_FDDD(gsl_cdf_flat_Q,"cdf_flat_Q")
D_FDDD(gsl_cdf_flat_Qinv,"cdf_flat_Qinv")
D_FDDD(gsl_cdf_gamma_P,"cdf_gamma_P")
D_FDDD(gsl_cdf_gamma_Pinv,"cdf_gamma_Pinv")
D_FDDD(gsl_cdf_gamma_Q,"cdf_gamma_Q")
D_FDDD(gsl_cdf_gamma_Qinv,"cdf_gamma_Qinv")
D_FDDD(gsl_cdf_gumbel1_P,"cdf_gumbel1_P")
D_FDDD(gsl_cdf_gumbel1_Pinv,"cdf_gumbel1_Pinv")
D_FDDD(gsl_cdf_gumbel1_Q,"cdf_gumbel1_Q")
D_FDDD(gsl_cdf_gumbel1_Qinv,"cdf_gumbel1_Qinv")
D_FDDD(gsl_cdf_gumbel2_P,"cdf_gumbel2_P")
D_FDDD(gsl_cdf_gumbel2_Pinv,"cdf_gumbel2_Pinv")
D_FDDD(gsl_cdf_gumbel2_Q,"cdf_gumbel2_Q")
D_FDDD(gsl_cdf_gumbel2_Qinv,"cdf_gumbel2_Qinv")
D_FDDD(gsl_cdf_lognormal_P,"cdf_lognormal_P")
D_FDDD(gsl_cdf_lognormal_Pinv,"cdf_lognormal_Pinv")
D_FDDD(gsl_cdf_lognormal_Q,"cdf_lognormal_Q")
D_FDDD(gsl_cdf_lognormal_Qinv,"cdf_lognormal_Qinv")
D_FDDD(gsl_cdf_pareto_P,"cdf_pareto_P")
D_FDDD(gsl_cdf_pareto_Pinv,"cdf_pareto_Pinv")
D_FDDD(gsl_cdf_pareto_Q,"cdf_pareto_Q")
D_FDDD(gsl_cdf_pareto_Qinv,"cdf_pareto_Qinv")
D_FDDD(gsl_cdf_weibull_P,"cdf_weibull_P")
D_FDDD(gsl_cdf_weibull_Pinv,"cdf_weibull_Pinv")
D_FDDD(gsl_cdf_weibull_Q,"cdf_weibull_Q")
D_FDDD(gsl_cdf_weibull_Qinv,"cdf_weibull_Qinv")
D_FDD(gsl_cdf_cauchy_P,"cdf_cauchy_P")
D_FDD(gsl_cdf_cauchy_Pinv,"cdf_cauchy_Pinv")
D_FDD(gsl_cdf_cauchy_Q,"cdf_cauchy_Q")
D_FDD(gsl_cdf_cauchy_Qinv,"cdf_cauchy_Qinv")
D_FDD(gsl_cdf_chisq_P,"cdf_chisq_P")
D_FDD(gsl_cdf_chisq_Pinv,"cdf_chisq_Pinv")
D_FDD(gsl_cdf_chisq_Q,"cdf_chisq_Q")
D_FDD(gsl_cdf_chisq_Qinv,"cdf_chisq_Qinv")
D_FDD(gsl_cdf_exponential_P,"cdf_exponential_P")
D_FDD(gsl_cdf_exponential_Pinv,"cdf_exponential_Pinv")
D_FDD(gsl_cdf_exponential_Q,"cdf_exponential_Q")
D_FDD(gsl_cdf_exponential_Qinv,"cdf_exponential_Qinv")
D_FDD(gsl_cdf_gaussian_P,"cdf_gaussian_P")
D_FDD(gsl_cdf_gaussian_Pinv,"cdf_gaussian_Pinv")
D_FDD(gsl_cdf_gaussian_Q,"cdf_gaussian_Q")
D_FDD(gsl_cdf_gaussian_Qinv,"cdf_gaussian_Qinv")
D_FDD(gsl_cdf_laplace_P,"cdf_laplace_P")
D_FDD(gsl_cdf_laplace_Pinv,"cdf_laplace_Pinv")
D_FDD(gsl_cdf_laplace_Q,"cdf_laplace_Q")
D_FDD(gsl_cdf_laplace_Qinv,"cdf_laplace_Qinv")
D_FDD(gsl_cdf_logistic_P,"cdf_logistic_P")
D_FDD(gsl_cdf_logistic_Pinv,"cdf_logistic_Pinv")
D_FDD(gsl_cdf_logistic_Q,"cdf_logistic_Q")
D_FDD(gsl_cdf_logistic_Qinv,"cdf_logistic_Qinv")
D_FDD(gsl_cdf_rayleigh_P,"cdf_rayleigh_P")
D_FDD(gsl_cdf_rayleigh_Pinv,"cdf_rayleigh_Pinv")
D_FDD(gsl_cdf_rayleigh_Q,"cdf_rayleigh_Q")
D_FDD(gsl_cdf_rayleigh_Qinv,"cdf_rayleigh_Qinv")
D_FDD(gsl_cdf_tdist_P,"cdf_tdist_P")
D_FDD(gsl_cdf_tdist_Pinv,"cdf_tdist_Pinv")
D_FDD(gsl_cdf_tdist_Q,"cdf_tdist_Q")
D_FDD(gsl_cdf_tdist_Qinv,"cdf_tdist_Qinv")
D_FD(gsl_cdf_ugaussian_P,"cdf_ugaussian_P")
D_FD(gsl_cdf_ugaussian_Pinv,"cdf_ugaussian_Pinv")
D_FD(gsl_cdf_ugaussian_Q,"cdf_ugaussian_Q")
D_FD(gsl_cdf_ugaussian_Qinv,"cdf_ugaussian_Qinv")

#define V SLANG_VOID_TYPE
static SLang_Intrin_Fun_Type Module_Intrinsics [] =
{
   MAKE_INTRINSIC_0("cdf_beta_P", SLF(gsl_cdf_beta_P), V),
   MAKE_INTRINSIC_0("cdf_beta_Pinv", SLF(gsl_cdf_beta_Pinv), V),
   MAKE_INTRINSIC_0("cdf_beta_Q", SLF(gsl_cdf_beta_Q), V),
   MAKE_INTRINSIC_0("cdf_beta_Qinv", SLF(gsl_cdf_beta_Qinv), V),
   MAKE_INTRINSIC_0("cdf_exppow_P", SLF(gsl_cdf_exppow_P), V),
   MAKE_INTRINSIC_0("cdf_exppow_Q", SLF(gsl_cdf_exppow_Q), V),
   MAKE_INTRINSIC_0("cdf_fdist_P", SLF(gsl_cdf_fdist_P), V),
   MAKE_INTRINSIC_0("cdf_fdist_Pinv", SLF(gsl_cdf_fdist_Pinv), V),
   MAKE_INTRINSIC_0("cdf_fdist_Q", SLF(gsl_cdf_fdist_Q), V),
   MAKE_INTRINSIC_0("cdf_fdist_Qinv", SLF(gsl_cdf_fdist_Qinv), V),
   MAKE_INTRINSIC_0("cdf_flat_P", SLF(gsl_cdf_flat_P), V),
   MAKE_INTRINSIC_0("cdf_flat_Pinv", SLF(gsl_cdf_flat_Pinv), V),
   MAKE_INTRINSIC_0("cdf_flat_Q", SLF(gsl_cdf_flat_Q), V),
   MAKE_INTRINSIC_0("cdf_flat_Qinv", SLF(gsl_cdf_flat_Qinv), V),
   MAKE_INTRINSIC_0("cdf_gamma_P", SLF(gsl_cdf_gamma_P), V),
   MAKE_INTRINSIC_0("cdf_gamma_Pinv", SLF(gsl_cdf_gamma_Pinv), V),
   MAKE_INTRINSIC_0("cdf_gamma_Q", SLF(gsl_cdf_gamma_Q), V),
   MAKE_INTRINSIC_0("cdf_gamma_Qinv", SLF(gsl_cdf_gamma_Qinv), V),
   MAKE_INTRINSIC_0("cdf_gumbel1_P", SLF(gsl_cdf_gumbel1_P), V),
   MAKE_INTRINSIC_0("cdf_gumbel1_Pinv", SLF(gsl_cdf_gumbel1_Pinv), V),
   MAKE_INTRINSIC_0("cdf_gumbel1_Q", SLF(gsl_cdf_gumbel1_Q), V),
   MAKE_INTRINSIC_0("cdf_gumbel1_Qinv", SLF(gsl_cdf_gumbel1_Qinv), V),
   MAKE_INTRINSIC_0("cdf_gumbel2_P", SLF(gsl_cdf_gumbel2_P), V),
   MAKE_INTRINSIC_0("cdf_gumbel2_Pinv", SLF(gsl_cdf_gumbel2_Pinv), V),
   MAKE_INTRINSIC_0("cdf_gumbel2_Q", SLF(gsl_cdf_gumbel2_Q), V),
   MAKE_INTRINSIC_0("cdf_gumbel2_Qinv", SLF(gsl_cdf_gumbel2_Qinv), V),
   MAKE_INTRINSIC_0("cdf_lognormal_P", SLF(gsl_cdf_lognormal_P), V),
   MAKE_INTRINSIC_0("cdf_lognormal_Pinv", SLF(gsl_cdf_lognormal_Pinv), V),
   MAKE_INTRINSIC_0("cdf_lognormal_Q", SLF(gsl_cdf_lognormal_Q), V),
   MAKE_INTRINSIC_0("cdf_lognormal_Qinv", SLF(gsl_cdf_lognormal_Qinv), V),
   MAKE_INTRINSIC_0("cdf_pareto_P", SLF(gsl_cdf_pareto_P), V),
   MAKE_INTRINSIC_0("cdf_pareto_Pinv", SLF(gsl_cdf_pareto_Pinv), V),
   MAKE_INTRINSIC_0("cdf_pareto_Q", SLF(gsl_cdf_pareto_Q), V),
   MAKE_INTRINSIC_0("cdf_pareto_Qinv", SLF(gsl_cdf_pareto_Qinv), V),
   MAKE_INTRINSIC_0("cdf_weibull_P", SLF(gsl_cdf_weibull_P), V),
   MAKE_INTRINSIC_0("cdf_weibull_Pinv", SLF(gsl_cdf_weibull_Pinv), V),
   MAKE_INTRINSIC_0("cdf_weibull_Q", SLF(gsl_cdf_weibull_Q), V),
   MAKE_INTRINSIC_0("cdf_weibull_Qinv", SLF(gsl_cdf_weibull_Qinv), V),
   MAKE_INTRINSIC_0("cdf_cauchy_P", SLF(gsl_cdf_cauchy_P), V),
   MAKE_INTRINSIC_0("cdf_cauchy_Pinv", SLF(gsl_cdf_cauchy_Pinv), V),
   MAKE_INTRINSIC_0("cdf_cauchy_Q", SLF(gsl_cdf_cauchy_Q), V),
   MAKE_INTRINSIC_0("cdf_cauchy_Qinv", SLF(gsl_cdf_cauchy_Qinv), V),
   MAKE_INTRINSIC_0("cdf_chisq_P", SLF(gsl_cdf_chisq_P), V),
   MAKE_INTRINSIC_0("cdf_chisq_Pinv", SLF(gsl_cdf_chisq_Pinv), V),
   MAKE_INTRINSIC_0("cdf_chisq_Q", SLF(gsl_cdf_chisq_Q), V),
   MAKE_INTRINSIC_0("cdf_chisq_Qinv", SLF(gsl_cdf_chisq_Qinv), V),
   MAKE_INTRINSIC_0("cdf_exponential_P", SLF(gsl_cdf_exponential_P), V),
   MAKE_INTRINSIC_0("cdf_exponential_Pinv", SLF(gsl_cdf_exponential_Pinv), V),
   MAKE_INTRINSIC_0("cdf_exponential_Q", SLF(gsl_cdf_exponential_Q), V),
   MAKE_INTRINSIC_0("cdf_exponential_Qinv", SLF(gsl_cdf_exponential_Qinv), V),
   MAKE_INTRINSIC_0("cdf_gaussian_P", SLF(gsl_cdf_gaussian_P), V),
   MAKE_INTRINSIC_0("cdf_gaussian_Pinv", SLF(gsl_cdf_gaussian_Pinv), V),
   MAKE_INTRINSIC_0("cdf_gaussian_Q", SLF(gsl_cdf_gaussian_Q), V),
   MAKE_INTRINSIC_0("cdf_gaussian_Qinv", SLF(gsl_cdf_gaussian_Qinv), V),
   MAKE_INTRINSIC_0("cdf_laplace_P", SLF(gsl_cdf_laplace_P), V),
   MAKE_INTRINSIC_0("cdf_laplace_Pinv", SLF(gsl_cdf_laplace_Pinv), V),
   MAKE_INTRINSIC_0("cdf_laplace_Q", SLF(gsl_cdf_laplace_Q), V),
   MAKE_INTRINSIC_0("cdf_laplace_Qinv", SLF(gsl_cdf_laplace_Qinv), V),
   MAKE_INTRINSIC_0("cdf_logistic_P", SLF(gsl_cdf_logistic_P), V),
   MAKE_INTRINSIC_0("cdf_logistic_Pinv", SLF(gsl_cdf_logistic_Pinv), V),
   MAKE_INTRINSIC_0("cdf_logistic_Q", SLF(gsl_cdf_logistic_Q), V),
   MAKE_INTRINSIC_0("cdf_logistic_Qinv", SLF(gsl_cdf_logistic_Qinv), V),
   MAKE_INTRINSIC_0("cdf_rayleigh_P", SLF(gsl_cdf_rayleigh_P), V),
   MAKE_INTRINSIC_0("cdf_rayleigh_Pinv", SLF(gsl_cdf_rayleigh_Pinv), V),
   MAKE_INTRINSIC_0("cdf_rayleigh_Q", SLF(gsl_cdf_rayleigh_Q), V),
   MAKE_INTRINSIC_0("cdf_rayleigh_Qinv", SLF(gsl_cdf_rayleigh_Qinv), V),
   MAKE_INTRINSIC_0("cdf_tdist_P", SLF(gsl_cdf_tdist_P), V),
   MAKE_INTRINSIC_0("cdf_tdist_Pinv", SLF(gsl_cdf_tdist_Pinv), V),
   MAKE_INTRINSIC_0("cdf_tdist_Q", SLF(gsl_cdf_tdist_Q), V),
   MAKE_INTRINSIC_0("cdf_tdist_Qinv", SLF(gsl_cdf_tdist_Qinv), V),
   MAKE_INTRINSIC_0("cdf_ugaussian_P", SLF(gsl_cdf_ugaussian_P), V),
   MAKE_INTRINSIC_0("cdf_ugaussian_Pinv", SLF(gsl_cdf_ugaussian_Pinv), V),
   MAKE_INTRINSIC_0("cdf_ugaussian_Q", SLF(gsl_cdf_ugaussian_Q), V),
   MAKE_INTRINSIC_0("cdf_ugaussian_Qinv", SLF(gsl_cdf_ugaussian_Qinv), V),
#ifdef _GSLSF_MODULE_C_
   MAKE_INTRINSIC_0("gslsf_get_precision", get_gsl_precision, SLANG_INT_TYPE),
   MAKE_INTRINSIC_I("gslsf_set_precision", set_gsl_precision, SLANG_VOID_TYPE),
#endif
   SLANG_END_INTRIN_FUN_TABLE
};
#undef V
#endif				       /* MODULE_HAS_INTRINSICS */

static SLang_Intrin_Var_Type Module_Variables [] =
{
   MAKE_VARIABLE("_gslcdf_module_version_string", &Module_Version_String, SLANG_STRING_TYPE, 1),
   MAKE_VARIABLE("GSL_VERSION", &gsl_version, SLANG_STRING_TYPE, 1),
   SLANG_END_INTRIN_VAR_TABLE
};

static SLang_IConstant_Type Module_IConstants [] =
{
   MAKE_ICONSTANT("_gslcdf_module_version", MODULE_VERSION_NUMBER),
#ifdef _GSLSF_MODULE_C_
   MAKE_ICONSTANT("GSL_PREC_SINGLE", GSL_PREC_SINGLE),
   MAKE_ICONSTANT("GSL_PREC_DOUBLE", GSL_PREC_DOUBLE),
   MAKE_ICONSTANT("GSL_PREC_APPROX", GSL_PREC_APPROX),
#endif
   SLANG_END_ICONST_TABLE
};

#ifdef MODULE_HAS_DCONSTANTS
static SLang_DConstant_Type Module_DConstants [] =
{
   SLANG_END_DCONST_TABLE
};
#endif

int init_gslcdf_module_ns (char *ns_name)
{
   SLang_NameSpace_Type *ns = SLns_create_namespace (ns_name);
   if (ns == NULL)
     return -1;

   if (
       (-1 == SLns_add_intrin_var_table (ns, Module_Variables, NULL))
#ifdef MODULE_HAS_INTRINSICS
       || (-1 == SLns_add_intrin_fun_table (ns, Module_Intrinsics, NULL))
#endif
       || (-1 == SLns_add_iconstant_table (ns, Module_IConstants, NULL))
#ifdef MODULE_HAS_DCONSTANTS
       || (-1 == SLns_add_dconstant_table (ns, Module_DConstants, NULL))
#endif
       )
     return -1;

   return 0;
}

/* This function is optional */
void deinit_gslcdf_module (void)
{
}
