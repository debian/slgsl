prepend_to_slang_load_path (".");
set_import_module_path (".:" + get_import_module_path ());

private define test_ns (module, ns)
{
   require (module, ns);
   if (-1 != is_defined ("$ns->_${module}_module_version"$))
     {
	() = fprintf (stderr, "*** ERROR: %s did not load into %s\n", module, ns);
     }
}

private variable Modules =
{
   "gslrand",
   "gslcdf",
   "gslsf",
   "gslmatrix",
   "gslconst",
   "gslfft",
   "gslinterp"
};

define slsh_main ()
{
   foreach (Modules)
     {
	variable module = ();
	loop (3) test_ns (module, "Global");
	loop (3) test_ns (module, "foo");
	loop (3) test_ns (module, "bar");
     }
}
