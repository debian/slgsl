prepend_to_slang_load_path (".");
set_import_module_path (".:" + get_import_module_path ());

require ("gslrand");

define test_gaussian ()
{
   variable a, b, r = rng_alloc ();

   rng_set (r, 0);
   rng_set (0);

   a = ran_ugaussian (r, 1000);
   if (length (a) != 1000)
     {
	() = fprintf (stderr, "Failed: got %d elements instead of 1000",
		      length (a));
	exit (1);
     }

   b = ran_ugaussian (1000);
   if (length (where (a != b)))
     {
	() = fprintf (stderr, "Failed: ran_ugaussian(1000)");
	exit (1);
     }

   a = ran_ugaussian (r, 1000);
   foreach (a)
     {
	b = ();
	if (b != ran_ugaussian ())
	  {
	     () = fprintf (stderr, "Failed: ran_ugaussian()");
	     exit (1);
	  }
     }

   a = ran_gaussian (r, 2.0, 1000);
   if (length (a) != 1000)
     {
	() = fprintf (stderr, "Failed2: got %d elements instead of 1000",
		      length (a));
	exit (1);
     }

   b = ran_gaussian (2, 1000);
   if (length (b) != 1000)
     {
	() = fprintf (stderr, "Failed3: got %d elements instead of 1000",
		      length (b));
	exit (1);
     }

   if (length (where (a != b)))
     {
	() = fprintf (stderr, "Failed: ran_ugaussian(1000)");
	exit (1);
     }
   
   a = ran_gaussian (r, 2.0, 1000);
   foreach (a)
     {
	b = ();
	if (b != ran_gaussian (2.0))
	  {
	     () = fprintf (stderr, "Failed: ran_gaussian(2.0)");
	     exit (1);
	  }
     }
}

test_gaussian ();
exit (0);

   
   
   
