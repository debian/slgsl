define _gslcore_import_module (name, ns)
{
   if (ns == "")
     ns = "Global";
   if (-1 == is_defined ("$ns->_${name}_module_version"$))
     return;			       %  already "imported"
   import ("gsl");
   (@__get_reference("gsl_import_module"))(name, ns);
}

provide("gslcore");
