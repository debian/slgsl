\function_sect{Fast Fourier Transform Routines}

\function{_gsl_fft_complex}
\synopsis{Perform an N-d FFT}
\usage{y = _gsl_fft_complex (x, dir)}
\description
  This routine computes the FFT of an array \exmp{x} and returns the
  result.  The integer-valued parameter \exmp{dir} parameter specifies
  the direction of the transform.  A forward transform will be
  produced for positive values of \exmp{dir} and a reverse transform
  will be computed for negative values.

  The result will be a complex array of the same size and
  dimensionality as the the input array.
\notes
  It is better to call this routine indirectly using the \sfun{fft}
  function.
\seealso{fft}
\done

\function{fft}
\synopsis{Perform an N-d FFT}
\usage{y = fft (x, dir)}
\description
  This routine computes the FFT of an array \exmp{x} and returns the
  result.  The integer-valued parameter \exmp{dir} parameter specifies
  the direction of the transform.  A forward transform will be
  produced for positive values of \exmp{dir} and a reverse transform
  will be computed for negative values.

  The result will be a complex array of the same size and
  dimensionality as the the input array.
\example
  Assume that the array \exmp{x} represents the values of a signal
  that is sampled at 10 Hz, i.e., with a period \exmp{T} of 0.1 secs.
  Then
#v+
    T = 0.1;
    N = length(x);
    y = fft (x, 1);
#v-
  will result in the \exmp{y} being set to the FFT of the samples
  \var{x}.  The frequencies represented by \var{y} contains both
  positive and negative frequencies from from \exmp{-1/2T} to
  \exmp{+1/2T}.  The frequency \exmp{f} represented by \exmp{y[i]} is
  given by \exmp{i/(NT)} for \exmp{i<=N/2}, and \exmp{(i-N))/NT} for
  \exmp{i>N/2}.  If \exmp{N} is even, then the frequency appearing at
  \exmp{i=N/2} represents both positive and negatives values of
  \exmp{1/2T}.  Programatically, this corresponds to
#v+
    f = [[0:N/2], [N/2+1-N:-1]]/double(N*T);
#v-
\notes
  This routine is currently a wrapper for the \exmp{_gsl_fft_complex}
  function.

\seealso{_gsl_fft_complex}
\done


\function{convolve}
\synopsis{Perform a convolution}
\usage{b = convolve (array, kernel)}
\description
  This function performs a convolution of the specified array and
  kernel using FFTs.  One of the following qualifiers may be used to
  indicate how the overlap of the kernel with the edges of the array
  are to be handled:
#v+
    pad=value        Pad the array with the specified value
    wrap             Use periodic boundary-conditions
    reflect          Reflect the pixels at the boundary
    nearest          Use the value of the nearest edge pixel
#v-
  The default behavior is to use pad=0.0.
\notes
  The current implementation utilizes ffts and will expand the array
  to the nearest multiple of small primes for run-time efficiency.  A
  future version may allow different methods to be used.
\seealso{fft, correlate}
\done

\function{correlate}
\synopsis{Perform a correlation}
\usage{b = correlate (array, kernel)}
\description
  This function performs a correlation of the specified array and
  kernel using FFTs.  One of the following qualifiers may be used to
  indicate how the overlap of the kernel with the edges of the array
  are to be handled:
#v+
    pad=value        Pad the array with the specified value
    wrap             Use periodic boundary-conditions
    reflect          Reflect the pixels at the boundary
    nearest          Use the value of the nearest edge pixel
#v-
  The default behavior is to use pad=0.0.
\notes
  The current implementation utilizes ffts and will expand the array
  to the nearest multiple of small primes for run-time efficiency.  A
  future version may allow different methods to be used.  
\seealso{fft, convolve}
\done
